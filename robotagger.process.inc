<?php

/**
 * @file Request the RoboTagger-API and saves the annotation suggestions, detected topic to the fields or
 * saves, declines, deletes the suggestions in the database.
 */

/**
 * A wrapper function.
 *
 * @param stdClass $node
 *   The node object.
 */
function _robotagger_process_node(stdClass $node) {
  if ($node->robotagger_process_all) {
    _robotagger_process_node_nodeapi($node);
  }
  elseif (!$node->robotagger_process_all && $node->rtg_op == 'presave') {
    _robotagger_process_node_form($node);
  }
}

/**
 * Requests the RoboTagger-API and saves the annotations.
 *
 * @param stdClass $node
 *   The node object.
 */
function _robotagger_process_node_nodeapi(stdClass $node) {
  $langcode = (!empty($node->language) && $node->language != 'und') ? $node->language : language_default()->language;
  $settings = _robotagger_get_settings($node->type);
  /* On presave we saves the annotation in th node object.
  On insert/update in our database. Therefore we need to avoid thats the function
  runs twice.
  */
  if ($node->rtg_op != 'presave' && $settings->confirmtags == 'auto') {
    return NULL;
  }
  if ($node->rtg_op == 'presave' && $settings->confirmtags == 'manual') {
    return NULL;
  }

  if ($settings->sendrequest == 'disabled' || !$settings->isallow) {
    return NULL;
  }
  if ($settings->sendrequest == 'manual' && $node->rtcheck_sendrequest == TRUE) {
    return NULL;
  }
  //remove disabled vids
  $vids = array_filter($settings->vids);
  if (empty($vids)) {
    return NULL;
  }
  $instances = field_info_instances('node', $node->type);
  $machine_names = $vocabulary_fieldnames = array();
  foreach ($instances as $instance) {
    if (!$instance['deleted'] && isset($instance['settings']['vocabulary'])) {
      $field_name = _robotagger_prepare_voc_fieldname($instance['settings']['vocabulary']);
      if ($field_name == $instance['field_name']) {
        $machine_names[$instance['settings']['vocabulary']] = $instance['settings']['vocabulary'];
      }
    }
  }
  if (!empty($machine_names)) {
    $nodetype_vids = array();
    $query = db_select('taxonomy_vocabulary', 'tv')
      ->fields('tv')
      ->condition('tv.machine_name', $machine_names, 'IN');
    $results = $query->execute();
    foreach ($results as $vocabulary) {
      if (!empty($vids[$vocabulary->vid])) {
        $nodetype_vids[$vocabulary->vid] = $vocabulary->name;
        $vocabulary_fieldnames[$vocabulary->vid] = _robotagger_prepare_voc_fieldname($vocabulary);
      }
    }
    $text = '';
    foreach ($settings->supported_fields as $fieldname) {
      if (isset($node->{$fieldname})) {
        foreach ($node->{$fieldname} as $lang) {
          foreach ($lang as $item) {
            $text .= $item['value'] . ' ';
          }
        }
      }
    }
    if (empty($text)) {
      return NULL;
    }
    if ($settings->use_title) {
      $text = $node->title . ' ' . $text;
    }
    if (!empty($nodetype_vids)) {
      $data = robotagger_api_call_webservice($text, $nodetype_vids, $langcode, $settings->caching);
      if (empty($data)) {
        return NULL;
      }
      $topic = (empty($data['topic']) || $data['topic'] == 'nicht erkannt') ? '' : $data['topic'];
      _robotagger_topic_save($node, $topic);
      if ($settings->logging) {
        watchdog('robotagger', 'Robotagger processing %title.', array('%title' => truncate_utf8($text, 150, TRUE, TRUE)));
      }
      $terms = _robotagger_taxonomy_node_get_terms($node, $key = 'tid');
      $declined = _robotagger_get_declined_suggestions($node);
      $oldtids = $newsuggestions = array();
      foreach ($nodetype_vids as $vid => $vocname) {
        if (isset($data[$vocname])) {
          foreach ($data[$vocname] as &$item) {
            if (($value = trim($item['value'])) && !empty($value)) {
              // Is the suggestion already in our declined suggestions?
              if (!isset($declined[$vid][$value])) {
                // Does taxonomy know this
                $tid = _robotagger_check_term_is_new($value, $vid);
                if (empty($tid)) {
                  $newsuggestions[$vid][$value] = $item;
                }
                elseif (!empty($tid) && !isset($terms[$tid])) {
                  $oldtids[$vid][] = array('tid' => $tid);
                  // on the confirmation page we also provide suggestions, which
                  // already known as term
                  if ($settings->confirmtags == 'manual') {
                    $newsuggestions[$vid][$value] = $item;
                  }
                }
              }
            }
          }
        }
      }// end foreach ($nodetypes_vids as $vid => $vocname) {
      // only in presave we can add term fields
      if ($node->rtg_op == 'presave' && $settings->confirmtags == 'auto') {
        _robotagger_process_node_suggestions($node, $newsuggestions, $oldtids, $vocabulary_fieldnames);
      }
      elseif ($node->rtg_op != 'presave' && $settings->confirmtags == 'manual') {
        _robotagger_suggestions_save($node, $newsuggestions);
      }
    }
  }
}

/**
 * Creates taxonomy terms for new suggestions and saves the new created terms or
 * already stored terms in the node object. Works only presave mode.
 *
 * @param object $node
 *   The node object.
 * @param array $newsuggestions
 *   An array of annotation suggestions.
 * @param array $oldtids
 *   An array of tids for a certain vocabulary.
 * @param array $vocabulary_fieldnames
 *   An array of vocabularies machnine names as values and as key the vocabulary id.
 *
 * @return object $node
 *   The altered node object.
 */
function _robotagger_process_node_suggestions($node, $newsuggestions = array(), $oldtids = array(), $vocabulary_fieldnames = NULL) {
  if (empty($vocabulary_fieldnames)) {
    return NULL;
  }
  if (!empty($newsuggestions)) {
    foreach ($newsuggestions as $vid => $items) {
      foreach ($items as $item) {
        $term = new stdClass;
        $term->vid = $vid;
        $term->name = $item['value'];
        $tid = taxonomy_term_save($term);
        if (!empty($tid)) {
          $oldtids[$vid][] = array('tid' => $term->tid);
        }
      }
    }
  }
  foreach ($oldtids as $vid => $terms) {
    $field_name = $vocabulary_fieldnames[$vid];
    if (empty($node->{$field_name}[LANGUAGE_NONE])) {
      $node->{$field_name}[LANGUAGE_NONE] = array();
    }
    $node->{$field_name}[LANGUAGE_NONE] = array_merge($node->{$field_name}[LANGUAGE_NONE], $terms);
  }
  return $node;
}

/**
 * Checks if a annotation suggestion is already in the pool of terms of
 * a vocabulary.
 *
 * @param string $name
 *   The annotation suggestion.
 * @param int $vid
 *   The vocabulary id.
 *
 * @return $tid
 *   The tid if the suggestion is known or NULL.
 */
function _robotagger_check_term_is_new($name, $vid) {
  $tid = NULL;
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'taxonomy_term');
  $query->propertyCondition('name', $name);
  $query->propertyCondition('vid', $vid);
  $results = $query->execute();
  if (!empty($results)) {
    $tids = array_keys($results['taxonomy_term']);
    $tid = $tids[0];
  }
  return $tid;
}

/**
 * Prepares the data for _robotagger_process_node_suggestions().
 *
 * @param object $node
 *   The node object.
 *
 * @see robotagger_node_page_form_submit().
 *   The property robotagger_suggestions of the node object is set by
 *   this submit handler.
 */
function _robotagger_process_node_form($node) {
  if (!empty($node->robotagger_suggestions)) {
    $vids = $newsuggestions = $vocabulary_fieldnames = $oldtids = $cleanupsuggestions = array();
    $nodeterms = _robotagger_taxonomy_node_get_terms($node);
    foreach ($node->robotagger_suggestions as $vid => $suggestions) {
      $vids[] = $vid;
      foreach ($suggestions as $suggestion) {
        // Does taxonomy known this annotation suggestion
        $tid = _robotagger_check_term_is_new($suggestion, $vid);
        if (empty($tid)) {
          $newsuggestions[$vid][$suggestion] = array('value' => $suggestion);
        }
        elseif (empty($nodeterms[$tid])) {
          $oldtids[$vid][] = array('tid' => $tid);
        }
        elseif (!empty($nodeterms[$tid])) {
          $cleanupsuggestions[$vid][$suggestion] = $suggestion;
        }
      }
    }
    $query = db_select('taxonomy_vocabulary', 'tv')
    ->fields('tv')
    ->condition('tv.vid', $vids, 'IN');
    $results = $query->execute();
    if (!empty($results)) {
      foreach ($results as $result) {
        $vocabulary_fieldnames[$result->vid] = _robotagger_prepare_voc_fieldname($result);
      }
    }
    _robotagger_process_node_suggestions($node, $newsuggestions, $oldtids, $vocabulary_fieldnames);
    $cleanupsuggestions += $node->robotagger_suggestions;
    _robotagger_suggestions_delete($node, $cleanupsuggestions);
  }
  return;
}

/**
 * Saves the suggestions in the robotagger db table, so that the user can confirm the suggestions.
 *
 * @param object $node
 *   The node object.
 * @param int $vid
 *   The vocabulary id.
 * @param array $newsuggestions
 *   An array of new suggestions.
 */
function _robotagger_suggestions_save($node, $newsuggestions) {
  if (isset($node->nid)) {
    $new = FALSE;
    $saved_suggestions = _robotagger_get_suggestions($node);
    foreach ($newsuggestions as $vid => $items) {
      foreach ($items as $item) {
        if (isset($saved_suggestions[$vid][$item['value']])) {
          continue;
        }
        db_insert('robotagger_suggestions')
        ->fields(array(
          'nid' => $node->nid,
          'nvid' => $node->vid,
          'vid' => $vid,
          'annotation' => $item['value']))
        ->execute();
        $new = TRUE;
      }
    }
    //Do not show on our confirmation page.
    if ($new && arg(2) != 'robotagger') {
      $link = l(t('new suggestions'), 'node/' . $node->nid . '/robotagger');
      drupal_set_message(t('The RoboTagger web service was requested. Please confirm the returned !new_tags', array('!new_tags' => $link)), 'status', FALSE);
    }
  }
}

/**
 * Deletes suggestions.
 *
 * @param object $node
 *   The node object.
 * @param array $suggestions
 *   An array of suggestions.
 */
function _robotagger_suggestions_delete($node, $suggestions) {
  if (!empty($suggestions) && !empty($node->nid)) {
    foreach ($suggestions as $vid => $names) {
      foreach ($names as $name) {
        db_delete('robotagger_suggestions')
        ->condition('nid', $node->nid)
        ->condition('nvid', $node->vid)
        ->condition('vid', $vid)
        ->condition('annotation', $name)
        ->execute();
      }
    }
  }
}

/**
 * Flags suggestions as declined.
 *
 * @param object $node
 *   The node object.
 */
function _robotagger_suggestions_decline($node) {
  if (!empty($node->robotagger_suggestions) && !empty($node->nid)) {
    foreach ($node->robotagger_suggestions as $vid => $names) {
      foreach ($names as $name) {
        db_update('robotagger_suggestions')
        ->fields(array('declined' => 1))
        ->condition('nid', $node->nid)
        ->condition('nvid', $node->vid)
        ->condition('vid', $vid)
        ->condition('annotation', $name)
        ->execute();
      }
    }
  }
}

/**
 * Saves the topic in the node object.
 *
 * @param object $node
 *   The node object.
 * @param string $topic
 *   The detected topic of a analyzed text.
 *
 * @return object $node
 *   The altered node object.
 */
function _robotagger_topic_save($node, $topic) {
  if (empty($topic)) {
    return $node;
  }
  if (isset($node->robotagger_topic)) {
    $node->robotagger_topic[LANGUAGE_NONE][0] = array('value' => $topic);
  }
  return $node;
}

/**
 * Simulate the old function taxonomy_node_get_terms() (Drupal 6).
 * @see http://drupal.org/node/909968#comment-4079906
 *
 * @param object $node
 *   The node object
 * @param string $key
 *   The term object property.
 *
 * @return array $terms
 *   An array of term objects.
 */
function _robotagger_taxonomy_node_get_terms($node, $key = 'tid') {
  static $terms;
  if (!isset($terms[$node->vid][$key])) {
    $query = db_select('taxonomy_index', 'r');
    $t_alias = $query->join('taxonomy_term_data', 't', 'r.tid = t.tid');
     $query->join('taxonomy_vocabulary', 'v', 't.vid = v.vid');
    $query->fields( $t_alias );
    $query->condition('r.nid', $node->nid);
    $results = $query->execute();
    $terms[$node->vid][$key] = array();
    foreach ($results as $term) {
      $terms[$node->vid][$key][$term->$key] = $term;
    }
  }
  return $terms[$node->vid][$key];
}
