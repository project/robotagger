;(function($) {
  Drupal.behaviors.RoboTaggerHelper = {
    attach : function(context, settings) {
      var $link = $('.robotagger-backend-js-select-all');
      var $sel = Drupal.t('Select all');
      var $dsel = Drupal.t('Deselect all');
      $link.click(function() {
        var $this = $(this);
        var $options = $this.parent().prev().find('input');
        if ($this.text() === $sel) {
          $options.attr('checked', true);
          $this.text($dsel);
        } else {
          $options.attr('checked', false);
          $this.text($sel);
        }
      });
    }
  };
})(jQuery);
