<?php

/**
 * @file Builds the settings page.
 */

function robotagger_settings_page() {
  $output = '';
  $form = drupal_get_form('robotagger_settings_form');
  $output .= drupal_render($form);
  return $output;
}

/**
 * @see robotagger_settings_page().
 */
function robotagger_settings_form($form, $form_state) {
  $apikey = variable_get('robotaggerapi_server_apikey', '');
  if (empty($apikey)) {
    drupal_set_message(t('Before you can continue, set the api key in the !link settings.', array('!link' => l('RoboTagger-Api', 'admin/config/robotagger/robotagger_api'))), 'warning');
    return $form;
  }
  $sendrequest_description = t('Automatic - After submitting the content, the module requests the RoboTagger web service. <br />Manual - You must send your content manually to the web service (on a seperated page).');
  $confirmtags_description = t('Automatic - The module saves the suggestions as taxonomy term immediately.<br />Manual - Before the suggestions are saved, you can confirm them (on a seperated page).');
  $form['#attached']['js'] = array(drupal_get_path('module', 'robotagger') . '/js/robotagger.js');
  $form['robotagger_global_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global settings'),
    '#description' => t('Please note. You need to create all field instances for each checked vocabulary in each checked content types manually. You may as well enable the checkbox %checkboxlabel in this form.', array('%checkboxlabel' => t('Create/delete field instances automatically?'))),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  );
  $form['robotagger_global_wrapper']['robotagger_global_sendrequest'] = array(
    '#type' => 'select',
    '#title' => t('Send request'),
    '#options' => array('disabled' => t('Disabled'), 'auto' => t('Automatic'), 'manual' => t('Manual')),
    '#default_value' => variable_get('robotagger_global_sendrequest', 'auto'),
    '#description' => $sendrequest_description,
  );
  $form['robotagger_global_wrapper']['robotagger_global_confirmtags'] = array(
    '#type' => 'select',
    '#title' => t('Confirm returned suggestions'),
    '#options' => array('auto' => t('Automatic'), 'manual' => t('Manual')),
    '#default_value' => variable_get('robotagger_global_confirmtags', 'auto'),
    '#description' => $confirmtags_description,
  );
  $form['robotagger_global_wrapper']['robotagger_global_caching'] = array(
    '#type' => 'select',
    '#title' => t('Caching returned data'),
    '#options' => array(TRUE => t('yes'), FALSE => t('no')),
    '#default_value' => variable_get('robotagger_global_caching', TRUE),
    '#description' => t('When enabled, the RoboTagger-API caches the returned data by the RoboTagger web service.'),
  );
  $form['robotagger_global_wrapper']['robotagger_global_logging'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable logging'),
    '#default_value' => variable_get('robotagger_global_logging', 1),
    '#description' => t('When checked, the RoboTagger module logs all requests to the RoboTagger web service.'),
  );
  $nodetypes = node_type_get_names();
  $form['robotagger_global_wrapper']['robotagger_global_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Supported content types'),
    '#options' => $nodetypes,
    '#default_value' => variable_get('robotagger_global_nodetypes', array()),
    '#description' => t('Choose the content types for the RoboTagger web service.'),
  );
  $form['robotagger_global_wrapper']['robotagger_global_use_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the title of the content'),
    '#default_value' => variable_get('robotagger_global_use_title', 1),
    '#description' => t('When enabled, the title will also be used for the RoboTagger web service. But it will be only used if at least one another text field is checked.'),
  );
  $options = $options_by_ct = $labels = array();
  $key = "robotagger_global_supported_fields";
  foreach ($nodetypes as $mname => $uiname) {
    $instances = field_info_instances('node', $mname);
    foreach ($instances as $instance) {
      if ($instance['widget']['module'] == 'text' && !$instance['deleted'] && $instance['field_name'] != 'robotagger_topic') {
        $labels[$instance['label']] = $instance['label'];
        $label = (count($labels) > 1) ? implode('/ ', $labels) : $labels[$instance['label']];
        $options[$instance['field_name']] = $label . ' (' . $instance['field_name'] . ')';
        $options_by_ct[$mname][$instance['field_name']] = $instance['label'] . ' (' . $instance['field_name'] . ')';
      }
    }
  }
  $form['robotagger_global_wrapper'][$key] = array(
    '#type' => empty($options) ? 'item' : 'checkboxes',
    '#title' => t('Fields'),
    '#options' => $options,
    '#default_value' => variable_get($key, array('title', 'body')),
    '#markup' => t('No supported fields from the text module are available.'),
    '#description' => t('Which fields should be used for the RoboTagger web service?'),
    /*
      TODO
      Works not atm. see http://drupal.org/node/1057748
     '#states' => array(
      'visible' => array("input[name^='robotagger_global_nodetypes']" => array(
        'checked' => TRUE,
      )),
    ),*/
  );
  $form['robotagger_global_wrapper']['robotagger_fields_instances'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create/Delete field instances automatically?'),
    '#description' => t('After submitting the form, the module creates the field instances for the checked vocabularies in the checked content types. And deletes all field instances (see listed vocabularies below) from the unchecked content types.'),
  );
  $options = _robotagger_get_vocabularies('db');
  $values = variable_get('robotagger_global_vocs', array());
  $form['robotagger_global_wrapper']['robotagger_global_vocs'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Vocabularies'),
    '#options' => $options,
    '#default_value' => $values,
    '#description' => t('Which vocabularies should be used for the RoboTagger web service?')
  );
  $text = array_filter($values) ? t('Deselect all') : t('Select all');
  $form['robotagger_global_wrapper']['js'] = array(
    '#type' => 'item',
    '#markup' => '<a style="cursor: pointer;" class="robotagger-backend-js-select-all">' . $text . '</a>',
  );
  $form['robotagger_nodetype_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content type settings'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  /* TODO
  Works not atm. see http://drupal.org/node/1057748
    '#states' => array(
      'visible' => array("input[name^='robotagger_global_nodetypes']" => array(
        'checked' => TRUE,
      )),
    ),*/
  );

  foreach ($nodetypes as $mname => $uiname) {
    $w_key = 'robotagger_nodetype_' . $mname . '_wrapper';
    $form['robotagger_nodetype_wrapper'][$w_key] = array(
      '#type' => 'fieldset',
      '#title' => $uiname,
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
      '#states' => array(
        'visible' => array("input[name='robotagger_global_nodetypes[$mname]']" => array(
          'checked' => TRUE,
         )),
      ),
    );
    $s_key = 'robotagger_nodetype_' . $mname . '_settings';
    $form['robotagger_nodetype_wrapper'][$w_key][$s_key.'_use_global'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use default settings'),
      '#description' => t('When checked all made settings below will be ignored and the global settings will be used instead.'),
      '#default_value' => variable_get($s_key.'_use_global', 1),
    );
    $form['robotagger_nodetype_wrapper'][$w_key][$s_key.'_sendrequest'] = array(
      '#type' => 'select',
      '#title' => t('Send request'),
      '#options' => array('auto' => t('Automatic'), 'manual' => t('Manual')),
      '#default_value' => variable_get($s_key.'_sendrequest', 'auto'),
      '#description' => $sendrequest_description,
    );
    $form['robotagger_nodetype_wrapper'][$w_key][$s_key.'_confirmtags'] = array(
      '#type' => 'select',
      '#title' => t('Confirm returned suggestions'),
      '#options' => array('auto' => t('Automatic'), 'manual' => t('Manual')),
      '#default_value' => variable_get($s_key.'_confirmtags', 'auto'),
      '#description' => $confirmtags_description,
    );
    $form['robotagger_nodetype_wrapper'][$w_key][$s_key.'_caching'] = array(
      '#type' => 'select',
      '#title' => t('Caching returned data'),
      '#options' => array(TRUE => t('yes'), FALSE => t('no')),
      '#default_value' => variable_get($s_key.'_caching', TRUE),
    );
    $form['robotagger_nodetype_wrapper'][$w_key][$s_key.'_use_title'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use the title of the content'),
      '#default_value' => variable_get($s_key.'_use_title', 1),
      '#description' => t('When enabled, the title will be also used for the RoboTagger web service.'),
    );
    $form['robotagger_nodetype_wrapper'][$w_key][$s_key.'_supported_fields'] = array(
      '#type' => empty($options_by_ct[$mname]) ? 'item' : 'checkboxes',
      '#title' => t('Field instances'),
      '#options' => empty($options_by_ct[$mname]) ? NULL : $options_by_ct[$mname],
      '#markup' => t('No supported field instances from the text module are available.'),
      '#description' => t('Which field instances should be used for the RoboTagger web service?'),
      '#default_value' => variable_get($s_key.'_supported_fields', array('body')),
    );
  }
  $form['#submit'][] = 'robotagger_settings_form_submit';
  return system_settings_form($form);
}

/**
 * Submit handler for our settings form.
 *
 * @param array $form
 * @param array $form_state
 *
 * @see robotagger_settings_form().
 */
function robotagger_settings_form_submit($form, $form_state) {
  $nodetypes = $form_state['values']['robotagger_global_nodetypes'];
  if ($form_state['values']['robotagger_fields_instances']) {
    $allowed_vocs = $form_state['values']['robotagger_global_vocs'];
    $vocabularies = taxonomy_get_vocabularies();
    foreach ($nodetypes as $nodetype => $allowed) {
      //When checked than created else remove all field instanced.
      if ($nodetype === $allowed) {
        _robotagger_create_instances($nodetype, $allowed_vocs);
      }
      else{
        _robotagger_remove_instances($nodetype, $vocabularies);
      }
    }
  }
}