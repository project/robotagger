<?php

/**
 * @file Builds the page for the path node/%/robotagger.
 */

/**
 * Returns the output to confirm/decline annotation suggestion and
 * send manually send a request to the webservice.
 *
 * @param object $node
 *   The node object.
 *
 * @return string $output
 *   The rendered page.
 */
function robotagger_node_page($node) {
  $output = '';
  $suggestions = _robotagger_get_suggestions($node);
  $declined = _robotagger_get_declined_suggestions($node);
  if (!empty($suggestions) || !empty($declined)) {
    $form = drupal_get_form('robotagger_node_page_form', $node, $suggestions, $declined);
    $output .= drupal_render($form);
  }
  $settings = _robotagger_get_settings($node->type);
  if ($settings->sendrequest == 'manual') {
    $form = drupal_get_form('robotagger_sendrequest_form', $node);
    $output .= drupal_render($form);
  }
  return $output;
}

/**
 * Builds the form where the user can confirm/decline annotation suggestions.
 *
 * @param array $form
 * @param array $form_state
 * @param array $node
 *   The node object.
 * @param array $suggestions
 *   An array of suggestions.
 * @param array $declined
 *   An array of declined suggestions.
 *
 * @see _robotagger_get_suggestions().
 * @see _robotagger_get_declined_suggestions().
 *
 * @return array $form
 *   The built form.
 */
function robotagger_node_page_form($form, $form_state, $node, $suggestions_by_vid, $declined) {
  $form['#tree'] = TRUE;
  $form['#node'] = $node;
  $temp = array();
  if (!empty($suggestions_by_vid)) {
    $form['vids']['caption'] = array(
      '#markup' => '<h2>' . t('Suggestions') . '</h2>',
    );
    foreach ($suggestions_by_vid as $vid => $suggestions) {
      $voc = taxonomy_vocabulary_load($vid);
      $header = array(
        'title' => array('data' => $voc->name),
      );
      $options = array();
      foreach ($suggestions as $suggestion) {
        $options[$suggestion] = array(
          'title' => array(
            'data' => array(
              '#title' => $suggestion,
              '#markup' => check_plain($suggestion),
            )
          ),
        );
      }
      // @see http://api.drupal.org/api/drupal/includes--form.inc/function/theme_tableselect/7#comment-23003
      $form['vids'][$vid] = array(
        '#type' => 'tableselect',
        '#options' => $options,
        '#header' => $header,
        '#attributes' => array(
          'class' => array(
            'tableheader-processed'
          ),
          'style' => 'display: inline;'
        ),
      );
    }
  }

  if (!empty($declined)) {
    $form['declined']['caption'] = array(
      '#markup' => '<h2>' . t('Declined suggestions') . '</h2>',
    );
    foreach ($declined as $vid => $suggestions) {
      $voc = taxonomy_vocabulary_load($vid);
      $header = array(
        'title' => array('data' => $voc->name),
      );
      $options = array();
      foreach ($suggestions as $suggestion) {
        $options[$suggestion] = array(
          'title' => array(
            'data' => array(
              '#title' => $suggestion,
              '#markup' => check_plain($suggestion),
            )
          ),
        );
      }
      // @see http://api.drupal.org/api/drupal/includes--form.inc/function/theme_tableselect/7#comment-23003
      $form['declined'][$vid] = array(
        '#type' => 'tableselect',
        '#options' => $options,
        '#header' => $header,
        '#attributes' => array(
          'class' => array(
            'tableheader-processed'
          ),
          'style' => 'display: inline;'
        ),
      );
    }
  }
  $form['action'] = array(
    '#type' => 'select',
    '#options' => array('confirm' => t('Confirm'), 'decline' => t('Decline')),
  );
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#attributes' => array('style' => 'display: block;'),
  );
  return $form;
}

/**
 * Submit handler for our confirmation form.
 *
 * @param array $form
 * @param array $form_state
 *
 * @see robotagger_node_page_form().
 */
function robotagger_node_page_form_submit($form, &$form_state) {
  include_once dirname(__FILE__).'/robotagger.process.inc';
  $node = $form['#node'];
  $node->robotagger_process_all = FALSE;
  $node->robotagger_suggestions = array();
  $newsuggestions = array();
  if (empty($form_state['values']['vids'])) {
    $form_state['values']['vids'] = array();
  }
  if (empty($form_state['values']['declined'])) {
    $form_state['values']['declined'] = array();
  }
  $submitted = $form_state['values']['vids'] + $form_state['values']['declined'];
  if (!empty($submitted)) {
    foreach ($submitted as $vid => $annotypes) {
      $annotypes = array_filter($annotypes);
      if (!empty($annotypes)) {
        $newsuggestions[$vid] = $annotypes;
      }
    }
    $node->robotagger_suggestions = $newsuggestions;
  }
  if (!empty($node->robotagger_suggestions)) {
    $form_state['redirect'] = 'node/' . $node->nid;
    if ($form_state['input']['action'] == 'confirm') {
      drupal_set_message(t('The annotation suggestion have been confirmed.'));
      node_save($node);
      return;
    }
    elseif ($form_state['input']['action'] == 'decline') {
      drupal_set_message(t('The annotation suggestion have been declined.'));
      _robotagger_suggestions_decline($node);
      return;
    }
  }
}

/**
 * Build the form with a button to requests the webservice manually.
 *
 * @param array $form
 * @param array $form_state
 * @param array $node
 *   The node object.
 *
 * @return array $form
 *   The built form.
 */
function robotagger_sendrequest_form($form, $form_state, $node) {
  $form['#node'] = $node;
  $form['desc'] = array(
    '#type' => 'item',
    '#markup' => t('Click the button to send your current content to the RoboTagger web service.'),
  );
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Send Request'),
  );
  return $form;
}

/**
 * Submit handler for our "Send Request" button form.
 *
 * @param array $form
 * @param array $form_state
 *
 * @see robotagger_sendrequest_form().
 */
function robotagger_sendrequest_form_submit($form, $form_state) {
  $node = (object)$form['#node'];
  $node->rtcheck_sendrequest = FALSE;
  node_save($node);
}
